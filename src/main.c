//*****************************************************************************
//
// MSP432 main.c template - Empty main
//
//****************************************************************************

#include "msp.h"
#include <math.h>

#include "system.h"
#include "hal_general.h"

#include "uart.h"
#include "spi.h"
//#include "nrf24.h"
#include "task.h"
#include "game.h"
#include "game_controller_host.h"
//#include "graphics.h"
#include "InvPM_Main.h"
#include "game_muh3.h"

//void debug_task(void);
//static graphics_t context;

//nrf24_t RF1;
//static void RF1_PollIRQ(void);

//extern void Game_nRF_Test_Init(void);

void main(void)
{
    SYS_Setup(); //Processor specific setup

    Timing_Init(); //Initialize timing module
    Task_Init(); //Initialize task module

    UART_Init(GAME_CONTROLLER_UART); //Initialize the UART channel to the controller
    UART_Init(SUBSYS_UART); //Initialize the UART channel to the terminal for debugging/game display

//    spi_settings_t nrf_spi;
//    nrf_spi.channel = RF_SPI_CH;
//    nrf_spi.bit_rate = 200000;
//    nrf_spi.hal_settings.char7bit = 0;
//    nrf_spi.hal_settings.msb_first = 1;
//    nrf_spi.mode = 0;
//    SPI_Init(&nrf_spi); //Initialize the SPI for the NRF Chip

    GameControllerHost_Init(); //Initialize the game controller host

    EnableInterrupts(); //Enable interrupts

    //Initialize games
    InvPM_Init();
    MUH3_Init();
//    Game_nRF_Test_Init();

//    RF1_CSN(1); //Enable the NRF Chip select
//    RF1.ce = RF1_CE;
//    RF1.csn = RF1_CSN;
//    RF1.spi_channel = RF_SPI_CH;
//    Game_NetworkInit(&RF1); //Initialize the RF Channel

//    context.screen_size = SCREEN_SIZE_128X64;
//    context.hal.uart_channel = SUBSYS_UART;
//    Graphics_Init(&context);
//    Task_Schedule(&debug_task, 0, 100, 500);

    while (1) {
        SystemTick(); //Run the task system
//        RF1_PollIRQ();
    }
}

// Method to poll the interrupt pin and see if an interrupt has occured
//void RF1_PollIRQ(void) {
//    static uint8_t pin_state = 1;
//    static uint32_t last = 0;
//    uint8_t new_state = RF1_GetIRQ();
//
//    if ((new_state != pin_state) && !new_state) {
//        last = TimeNow();
//        nRF24_ISR(&RF1);
//    } else if(!new_state && TimeSince(last)>5) {
//        last = TimeNow();
//        nRF24_ISR(&RF1);
//    }
//    pin_state = new_state;
//}

//void debug_task(void)
//{
//    static float fullCircle = 2*3.14159;
//    static float dtheta = 2*3.14159/24;
//    static float theta = 0;
//    static float rad = 15;
//
//    uint16_t originx = 64;
//    uint16_t originy = 32;
//    uint16_t destx = originx + rad*cosf(theta);
//    uint16_t desty = originy + rad*sinf(theta);
//
//    context.foreground[0] = 255;
//
//    Graphics_DrawLine(&context, (g_point_t){originx, originy}, (g_point_t){destx, desty});
//    Graphics_UpdateScreen(&context);
//
//    theta += dtheta;
//    if (theta > fullCircle) {
//        theta -= fullCircle;
//        Graphics_ClearScreen(&context);
//    }
//}
