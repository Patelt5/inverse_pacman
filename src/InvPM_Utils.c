/*
 * InvPM_Utils.c
 *
 *  Created on: Apr 10, 2017
 *      Author: tej
 */

#include "system.h"
#include "uart.h"
#include "InvPM_Main.h"
#include "InvPM_Utils.h"

uint8_t InvPM_OffsetX(uint8_t x, uint8_t dir)
{
    switch (dir) {
        case 1: return x+1;
        case 3: return x-1;
        default:
            return x;
    }
}

uint8_t InvPM_OffsetY(uint8_t y, uint8_t dir)
{
    switch (dir) {
        case 0: return y-1;
        case 2: return y+1;
        default:
            return y;
    }
}

coord_t InvPM_OffsetCoord(coord_t pos, uint8_t dir)
{
    return (coord_t){InvPM_OffsetX(pos.x, dir), InvPM_OffsetY(pos.y, dir)};
}

void InvPM_MoveCoord(coord_t* pos, uint8_t dir)
{
    pos->x = InvPM_OffsetX(pos->x, dir);
    pos->y = InvPM_OffsetY(pos->y, dir);
}

void InvPM_WaitForTXSpace(uint8_t channel, uint16_t size)
{
    while (UART_TXSpaceAvailable(channel) < size);
}
