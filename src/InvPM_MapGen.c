/*
 * InvPM_MapGen.c
 *
 *  Created on: Apr 24, 2017
 *      Author: tej
 */

#include <stdlib.h>
#include "system.h"

#include "InvPM_Main.h"
#include "InvPM_Utils.h"
#include "InvPM_MapGen.h"

#define GEN_MAX_BUILDERS GEN_MAX_SPAWNERS*4 //each spawner can spawn up to 4 builders

typedef struct {
    coord_t pos;
    uint8_t movesLeft;

    struct {
        uint8_t isAlive:1;
        uint8_t dir:3;
        uint8_t unused:4;
    };
} builder_t;

typedef struct {
    coord_t pos;

    struct {
        uint8_t dirMask:4;
        uint8_t isAlive:1;
        uint8_t unused:3;
    };
} spawner_t;

static struct {
    generator_settings_t* param;

    uint8_t numSpawners, numBuilders;

    spawner_t spawners[GEN_MAX_SPAWNERS];
    builder_t builders[GEN_MAX_BUILDERS];

    generator_space_t gen_space;
} gen_run;

static int randomInt(int min, int max);
static void clearGenRun();

static uint8_t isValidGenPos(coord_t pos);
static uint8_t isAvailableGenPos(coord_t pos);
static uint8_t isSpawnerInPos(coord_t pos);
static uint8_t isBuilderInPos(coord_t pos);
static coord_t getRandomAvailableGenPos();
static uint8_t areBuildersLeft();
static void turnBuilder(builder_t* builder);
static void refuelBuilder(builder_t* builder);

static void createSpawners();
static void spawnBuilders();
static void moveOrRedirectBuilders();
static void killFinishedBuilders();
static void carveMap();

generator_space_t* InvPM_GenerateMap(generator_settings_t* settings)
{
    clearGenRun(); //clear out the old values
    gen_run.param = settings; //set the settings
    srand(settings->seed); //set the seed for random generator

    createSpawners(); //create spawners at random positions
    spawnBuilders(); //spawn between 2-4 builders on top of spawners (each with different direction)

    do {
        moveOrRedirectBuilders(); //move builders forward and turn them as needed
        killFinishedBuilders(); //remove builders that hit already emptied space
        carveMap(); //empty out builders' new position
    } while (areBuildersLeft()); //repeat until all builders die

    return &gen_run.gen_space;
}

static void clearGenRun()
{
    gen_run.param = 0;
    gen_run.numBuilders = 0;
    gen_run.numSpawners = 0;

    volatile uint8_t i, j;
    for (i = 0; i < GEN_MAX_SPAWNERS; i++)
        gen_run.spawners[i].isAlive = 0;
    for (i = 0; i < GEN_MAX_BUILDERS; i++)
        gen_run.builders[i].isAlive = 0;

    for (i = 0; i < GEN_MAX_WIDTH; i++)
        for (j = 0; j < GEN_MAX_HEIGHT; j++)
            gen_run.gen_space.gen_space[i][j] = 1;
}

static void createSpawners()
{
    static uint8_t dirMasks[11] = {3, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15};

    uint8_t numSpawners = randomInt(gen_run.param->minSpawners, gen_run.param->maxSpawners); //decide on number of spawners for this run
    gen_run.numSpawners = numSpawners;

    volatile uint8_t i;
    for (i = 0; i < numSpawners; i++) { //for each spawner...
        spawner_t* s = &gen_run.spawners[i];
        s->isAlive = 1; //enable
        coord_t p = getRandomAvailableGenPos(); //get a random position without a spawner on it already
        s->pos.x = p.x; //set that position
        s->pos.y = p.y;
        s->dirMask = dirMasks[rand()%11]; //randomly decide which sides to spawn builders out of
        gen_run.gen_space.gen_space[p.x][p.y] = 0; //carve out this space
    }
}

static void spawnBuilders()
{
    volatile uint8_t numBuilders = 0;
    volatile uint8_t i, r;

    for (i = 0; i < gen_run.numSpawners; i++) { //for each spawner...
        spawner_t* s = &gen_run.spawners[i];

        for (r = 0; r < 4; r++) { //for each side r of the spawner
            if (numBuilders >= GEN_MAX_BUILDERS) return;

            if ((s->dirMask&1<<r) != 0) { //if we decided to create a builder to side r...
                coord_t pos2 = InvPM_OffsetCoord(s->pos, r);
                if (isAvailableGenPos(pos2)) { //if the side is within the map bounds
                    builder_t* b = &gen_run.builders[numBuilders++];
                    b->isAlive = 1; //create the builder
                    b->pos.x = s->pos.x; //set its position
                    b->pos.y = s->pos.y;
                    b->dir = r; //and its direction
                    refuelBuilder(b); //and refuel it
                }
            }
        }
    }

    gen_run.numBuilders = numBuilders; //keep track of how many builders we have
}

static void moveOrRedirectBuilders()
{
    volatile uint8_t i;
    for (i = 0; i < gen_run.numBuilders; i++) {
        builder_t* b = &gen_run.builders[i];
        if (b->isAlive) { //for every builder that is still alive...
            if (b->movesLeft <= 0) { //if no moves left
                turnBuilder(b);      //redirect the builder
                refuelBuilder(b);
            }

            coord_t pos2 = InvPM_OffsetCoord(b->pos, b->dir); //find the next position to move to

            if (!isValidGenPos(pos2)) { //if that next position is valid..
                turnBuilder(b);         //redirect the builder
                refuelBuilder(b);
                pos2 = InvPM_OffsetCoord(b->pos, b->dir); //get the new next position
            }

            b->pos.x = pos2.x; //move the builder
            b->pos.y = pos2.y;
            b->movesLeft--;    //reduce moves left
        }
    }
}

static void killFinishedBuilders()
{
    volatile uint8_t i;
    for (i = 0; i < gen_run.numBuilders; i++) {
        builder_t* b = &gen_run.builders[i];
        if (b->isAlive) { //for every builder that is still alive
            if (gen_run.gen_space.gen_space[b->pos.x][b->pos.y] == 0) //if the builder is on a previously carved spot...
                b->isAlive = 0; //kill the builder
        }
    }
}

static void carveMap()
{
    volatile uint8_t i;
    for (i = 0; i < gen_run.numBuilders; i++) {
        builder_t* b = &gen_run.builders[i];
        if (b->isAlive) { //for every builder that is still alive
            gen_run.gen_space.gen_space[b->pos.x][b->pos.y] = 0; //carve that position on the map
        }
    }
}

static int randomInt(int min, int max)
{
    return (rand()%(max+1-min)+min);
}

static uint8_t isValidGenPos(coord_t pos)
{
    if (pos.x >= gen_run.param->padding && pos.x <= gen_run.param->width-1-gen_run.param->padding)
        if (pos.y >= gen_run.param->padding && pos.y <= gen_run.param->height-1-gen_run.param->padding)
            return 1;
    return 0;
}

static uint8_t isAvailableGenPos(coord_t pos)
{
    return isValidGenPos(pos) && !isSpawnerInPos(pos) && !isBuilderInPos(pos);
}

static uint8_t isSpawnerInPos(coord_t pos)
{
    volatile uint8_t i;
    for (i = 0; i < gen_run.numSpawners; i++) {
        spawner_t* s = &gen_run.spawners[i];
        if (s->isAlive) {
            if (s->pos.x == pos.x && s->pos.y == pos.y)
                return 1;
        }
    }
    return 0;
}

static uint8_t isBuilderInPos(coord_t pos)
{
    volatile uint8_t i;
    for (i = 0; i < gen_run.numBuilders; i++) {
        builder_t* b = &gen_run.builders[i];
        if (b->isAlive) {
            if (b->pos.x == pos.x && b->pos.y == pos.y)
                return 1;
        }
    }
    return 0;
}

static coord_t getRandomAvailableGenPos()
{
    coord_t pos;

    do {
        pos.x = randomInt(gen_run.param->padding+2, gen_run.param->width-1-gen_run.param->padding-2);
        pos.y = randomInt(gen_run.param->padding+2, gen_run.param->height-1-gen_run.param->padding-2);

        pos.x += (pos.x+1)%2;
        pos.y += (pos.y+1)%2;
    } while (!isAvailableGenPos(pos));

    return pos;
}

static uint8_t areBuildersLeft()
{
    volatile uint8_t i;
    for (i = 0; i < gen_run.numBuilders; i++) {
        if (gen_run.builders[i].isAlive)
            return 1;
    }

    return 0;
}

static void turnBuilder(builder_t* builder)
{
    uint8_t dDir = rand()%2 ? 1 : 3; //pick left or right turn at random
    builder->dir = (builder->dir+dDir)%4; //set the new direction

    coord_t nextPos = InvPM_OffsetCoord(builder->pos, builder->dir); //get the next position
    if (!isValidGenPos(nextPos)) //If this turn is not valid...
        builder->dir = (builder->dir+2)%4; //turn to the other direction (opposite of dDir)
}

static void refuelBuilder(builder_t* builder)
{
    uint8_t moves = randomInt(gen_run.param->minBuilderTravelDist, gen_run.param->maxBuilderTravelDist); //randomly pick a distance
    moves += moves%2; //ensure distance is even

    builder->movesLeft = moves;
}

