/*
 * InvPM_Main.c
 *
 *  Created on: Apr 8, 2017
 *      Author: tej
 */

#include "system.h"
#include "uart.h"
#include "random_int.h"
#include "strings.h"
#include "game.h"
#include "timing.h"
#include "task.h"
#include "terminal.h"
#include "game_controller_host.h"
#include "game_controller.h"
#include "InvPM_Main.h"
#include "InvPM_Utils.h"
#include "InvPM_MapGen.h"

typedef enum {
    EMPTY,
    WALL,
} tile_state_e;

typedef enum {
    NONE,
    PELLET,
    POWER_PELLET,
    FRUIT
} drop_e;

typedef struct {
    tile_state_e state;
    drop_e drop;
    coord_t pos;
} map_tile_t;

typedef enum {
    TeamGhost,
    TeamPac
} team_e;

typedef struct {
    uint8_t id;
    coord_t pos;
    team_e team;

    uint8_t dir:4; //0 to 3 cw quart rotations from north, 4 for no dir
    uint8_t pressed_dir:4;

    uint8_t isVulnerable:1;
    uint8_t isAlive:1;
    uint8_t unused:6;
} player_t;

struct InversePacman {
    uint8_t id; //the game id

    uint8_t numOfPlayers; //number of players in this game
    player_t players[4]; //the players array (max 4 players)

    map_tile_t tiles[MAP_WIDTH][MAP_HEIGHT]; //the 2-D map matrix consisting of map_tile_t structs indexed by [x, y]

    team_e powerTeam; //the power team. Ghosts normally, pacmen during power pellet

    uint16_t secondsLeftInGame; //seconds left counter

    uint16_t score_pacmen;
    uint16_t score_ghosts;
} game;

generator_settings_t gen_setup = { //settings structure used to generate the map
    0, //seed
    MAP_WIDTH,
    MAP_HEIGHT,
    MAP_PADDING,

    MAP_MIN_INTRICACY, //min spawners
    MAP_MAX_INTRICACY, //max spawners

    MAP_MIN_CORRIDOR_DIST, //min dist
    MAP_MAX_CORRIDOR_DIST, //max dist
};

static void KeyboardReceiver(char c); //character receiver for keyboard control
static void ControllerReceiver(controller_buttons_t b, player_t* player); //controller receiver
static void Callback(int argc, char * argv[]); //argument callback for setting number of players
static void Play(void); //starts the game
static void Help(void); //shows the help info
static void GameOver(); //ends the game

static void PrintGUI(); //prints the score to the right of the map
static void DrawTileAtPos(coord_t pos); //draws the tile at the given position on the map
static void DrawTile(map_tile_t* tile); //draws the given tile

static player_t* GetPlayerAt(coord_t pos); //obtain a player at given position if exists
static uint8_t IsValidSpawnPos(coord_t pos); //check if pos is empty and no players are on it
static void RerenderAllPlayers(void); //reprints all players

static void SpawnPlayer(player_t* player, team_e team); //spawns a player on the given team in a random spot on the map
static void KillPlayer(player_t* player); //kills the given player until next respawn pass

static void MovePlayersOnTeam(team_e team); //moves all players on given team towards their facing direction
static void SetPlayerMoveDir(player_t* player, uint8_t dir); //set the move direction of the player if possible
static uint8_t CanPlayerMove(player_t* player, uint8_t dir); //check to see if the player can move towards given dir
static void TurnPlayerIfNecessary(player_t* player);
static void CheckCollisions(void); //check for opposing team collisions and kill the appropriate player
static void ConsumeOrCreateDrops(void); //pick up and put down drops for each player where necessary

static drop_e PlayerTouchedDrop(player_t* player, drop_e drop); //callback for when a player moves on a spot with a drop. returns the new drop
static drop_e PlayerCanAttemptDrop(player_t* player); //callback for when player could drop something. returns the new drop.

static void MovePacMen(void); //TASK: Forwards to MovePlayersOnTeam
static void MoveGhosts(void); //TASK: Forwards to MovePlayersOnTeam
static void RespawnAllPlayers(void); //TASK: Respawns all dead players
static void ResetGhostsToPower(void); //TASK: Resets the power pellet so ghosts are in power again
static void TickGameTimer(void); //TASK: decrements the seconds remaining variable and ends game if it reaches zero

void InvPM_Init(void)
{
    //Register the game with the management module
    game.id = Game_Register("InversePacman", "hey.", &Play, &Help);

    //Register a callback for changing number of players
    Game_RegisterCallback(game.id, &Callback);

    game.numOfPlayers = 3; // set default number of players

    //One time board initialization
    volatile int x, y;
    for (x = 0; x < MAP_WIDTH; x++)
        for (y = 0; y < MAP_HEIGHT; y++) {
            game.tiles[x][y].pos.x = x;
            game.tiles[x][y].pos.y = y;
        }
}

void Callback(int argc, char* argv[]) {
    if (argc >= 2) {
        if(strcasecmp(argv[0],"numplayers") == 0) { //command to set number of players in the game
            switch (argv[1][0]) {
                case '2':
                    game.numOfPlayers = 2;
                    Game_Log(game.id, "game set to 2 players");
                    return;
                case '3':
                    game.numOfPlayers = 3;
                    Game_Log(game.id, "game set to 3 players");
                    return;
                case '4':
                    game.numOfPlayers = 4;
                    Game_Log(game.id, "game set to 4 players");
                    return;
                default:
                    Game_Log(game.id, "invalid number of players: %c", argv[1][0]);
                    return;
            }
        } else {
            Game_Log(game.id, "command not supported: %s", argv[0]);
        }
    } else {
        Game_Log(game.id, "not enough arguments");
    }
}

void Help(void)
{
    Game_Printf("Pacman except all ghosts are player controlled.\r\n");
    Game_Printf("Ghosts score points by placing drops and killing the Pacman.\r\n");
    Game_Printf("Pacman scores points by eating drops and killing ghosts.\r\n");
    Game_Printf("All players respawn every %s seconds.\r\n", RESPAWN_PASS_INTERVAL/1000);
    Game_Printf("Game ends after %d seconds.\r\n", GAME_DURATION_SECONDS);
}

void Play(void)
{
    //Register the keyboard as receivers for both players
    Game_RegisterPlayer1Receiver(&KeyboardReceiver);

    //Optionally register controllers as the inputs for all players
    controller_buttons_t mask;
    mask.all_buttons = 0;
    mask.primary_buttons = 0xFF;
    volatile uint8_t i, j;
    for (i = 0; i < game.numOfPlayers; i++)
        GameControllerHost_RegisterPressCallback(i, (void (*)(controller_buttons_t, void *))&ControllerReceiver, mask, &game.players[i]);

    game.secondsLeftInGame = GAME_DURATION_SECONDS;
    game.score_pacmen = 0;
    game.score_ghosts = 0;

    Game_ClearScreen();
    Game_HideCursor();
    Game_SetColor(Reset_all_attributes);

    //Generate the map
    gen_setup.seed = TimeNow(); //set the seed of the generator
    generator_space_t* map = InvPM_GenerateMap(&gen_setup);
    for(i = 0; i < MAP_WIDTH; i++)
        for (j = 0; j < MAP_HEIGHT; j++) {
            if (map->gen_space[i][j]) {
                game.tiles[i][j].state = WALL;
                DrawTile(&game.tiles[i][j]);
            } else
                game.tiles[i][j].state = EMPTY;
        }

    //Spawn the players
    for (i = 0; i < game.numOfPlayers; i++) {
        game.players[i].id = i;
        SpawnPlayer(&game.players[i], i == 0 ? TeamPac : TeamGhost);
    }

    //Fill the board
    for (i = 1; i < MAP_WIDTH-1; i++)
        for (j = 1; j < MAP_HEIGHT-1; j++) {
            if (random_int(0, 9999)/9999.0 < PELLET_SPAWN_CHANCE && !GetPlayerAt((coord_t){i, j})) {
                if (game.tiles[i][j].state == EMPTY) {
                    game.tiles[i][j].drop = PELLET;
                    DrawTile(&game.tiles[i][j]);
                }
            }
        }

    PrintGUI(); //display the gui

    //Schedule the tasks
    Task_Schedule(&TickGameTimer, 0, 0, 1000);
    Task_Schedule(&MovePacMen, 0, 1, PAC_MOVEMENT_INTERVAL);
    Task_Schedule(&MoveGhosts, 0, 2, GHOST_MOVEMENT_INTERVAL);
    Task_Schedule(&RespawnAllPlayers, 0, 3, RESPAWN_PASS_INTERVAL);
}

static void DrawTileAtPos(coord_t pos)
{
    DrawTile(&game.tiles[pos.x][pos.y]);
}

static void DrawTile(map_tile_t* tile)
{
    player_t* player = GetPlayerAt(tile->pos);
    uint8_t x = tile->pos.x;
    uint8_t y = tile->pos.y;

    while(Game_IsTransmitting());

    if (player && player->isAlive) { //if there is a player on this spot...
        switch (player->team) {
            case TeamPac: //if there is a pacman on this spot
                Game_SetColor(Reset_all_attributes);
                Game_SetColor(ForegroundYellow);

                if (game.powerTeam == TeamPac)
                    Game_SetColor(Bright);
                else
                    Game_SetColor(Dim);

                Game_CharXY('@', x, y); //render the pacman
                return;
            case TeamGhost: //if there is a ghost on this spot
                Game_SetColor(Reset_all_attributes);
                if (player->isVulnerable == 0) {
                    Game_SetColor(Bright);
                    switch (player->id) {
                        case 1:
                            Game_SetColor(ForegroundCyan); break;
                        case 2:
                            Game_SetColor(ForegroundMagenta); break;
                        case 3:
                            Game_SetColor(ForegroundRed); break;
                        default:
                            Game_SetColor(ForegroundWhite);
                    }
                } else {
                    Game_SetColor(Dim);
                    Game_SetColor(BackgroundWhite);
                    Game_SetColor(ForegroundBlue);
                }

                Game_CharXY('#', x, y); //render the ghost
                return;
        }
    }

    switch (tile->state) {
        case WALL: //if this spot is a wall
            Game_SetColor(Reset_all_attributes);
            Game_SetColor(ForegroundWhite);
            Game_CharXY(219, x, y); //draw the wall character
            return;
    }

    switch (tile->drop) { //Render drop if exists
        case PELLET:
            Game_SetColor(Reset_all_attributes);
            Game_SetColor(ForegroundWhite);
            Game_CharXY('*', x, y);
            return;
        case FRUIT:
            Game_SetColor(Reset_all_attributes);
            Game_SetColor(ForegroundRed);
            Game_CharXY('%', x, y);
            return;
        case POWER_PELLET:
            Game_SetColor(Reset_all_attributes);
            Game_SetColor(ForegroundGreen);
            Game_CharXY('O', x, y);
            return;
    }

    Game_SetColor(Reset_all_attributes);
    Game_CharXY(' ', x, y); //render empty spot
}

static void KeyboardReceiver(char c)
{
    switch (c) { //recevier for first 3 players on WASD, TFGH, IJKL for players 1, 2, 3 respectively
        case 'W': case 'w': SetPlayerMoveDir(&game.players[1], 0); break;
        case 'A': case 'a': SetPlayerMoveDir(&game.players[1], 3); break;
        case 'S': case 's': SetPlayerMoveDir(&game.players[1], 2); break;
        case 'D': case 'd': SetPlayerMoveDir(&game.players[1], 1); break;

        case 'T': case 't': SetPlayerMoveDir(&game.players[3], 0); break;
        case 'F': case 'f': SetPlayerMoveDir(&game.players[3], 3); break;
        case 'G': case 'g': SetPlayerMoveDir(&game.players[3], 2); break;
        case 'H': case 'h': SetPlayerMoveDir(&game.players[3], 1); break;

        case 'I': case 'i': SetPlayerMoveDir(&game.players[2], 0); break;
        case 'J': case 'j': SetPlayerMoveDir(&game.players[2], 3); break;
        case 'K': case 'k': SetPlayerMoveDir(&game.players[2], 2); break;
        case 'L': case 'l': SetPlayerMoveDir(&game.players[2], 1); break;

        default:
            break;
    }
}

static void ControllerReceiver(controller_buttons_t b, player_t* player)
{
    if (b.button.up) {
        SetPlayerMoveDir(player, 0);
    } else if (b.button.left) {
        SetPlayerMoveDir(player, 3);
    } else if (b.button.down) {
        SetPlayerMoveDir(player, 2);
    } else if (b.button.right) {
        SetPlayerMoveDir(player, 1);
    }
}

static void SpawnPlayer(player_t* player, team_e team)
{
    player->team = team;
    player->isAlive = 1;
    player->isVulnerable = 0;
    player->dir = 4;
    player->pressed_dir = 4;

    uint8_t rx = random_int(0, MAP_WIDTH-1);
    uint8_t ry = random_int(0, MAP_HEIGHT-1);

    while (!IsValidSpawnPos((coord_t){rx, ry})) {
        rx = (rx+1)%MAP_WIDTH;
        if (rx == 0)
            ry = (ry+1)%MAP_HEIGHT;
    }

    player->pos.x = rx;
    player->pos.y = ry;

    DrawTileAtPos(player->pos);
}

static void RespawnAllPlayers()
{
    volatile uint8_t i;
    for (i = 0; i < game.numOfPlayers; i++) {
        if (!game.players[i].isAlive)
            SpawnPlayer(&game.players[i], game.players[i].team); //respawn the player with the same team
    }
}

static void RerenderAllPlayers()
{
    volatile uint8_t i;
    for (i = 0; i < game.numOfPlayers; i++) {
        if (game.players[i].isAlive)
            DrawTileAtPos(game.players[i].pos);
    }
}

static void ResetGhostsToPower()
{
    game.powerTeam = TeamGhost;
    volatile int i;
    for (i = 0; i < game.numOfPlayers; i++)
        if (game.players[i].isAlive && game.players[i].team == TeamGhost)
            game.players[i].isVulnerable = 0;
    RerenderAllPlayers(); //render the change in power position
}

static void KillPlayer(player_t* player)
{
    player->isAlive = 0;
    DrawTileAtPos(player->pos);

    switch (player->team) {
        case TeamPac:
            game.score_ghosts += GHOST_SCORE_PER_KILL;
            break;
        case TeamGhost:
            game.score_pacmen += PACMEN_SCORE_PER_KILL;
            break;
    }

    PrintGUI();
}

static uint8_t IsValidSpawnPos(coord_t pos)
{
    return game.tiles[pos.x][pos.y].state == EMPTY &&
            GetPlayerAt(pos) == 0; //Player can spawn on an empty tile with no other players on it.
}

static void MovePacMen(void)
{
    MovePlayersOnTeam(TeamPac);
}

static void MoveGhosts(void)
{
    MovePlayersOnTeam(TeamGhost);
}

static void MovePlayersOnTeam(team_e team)
{
    volatile uint8_t i;
    for (i = 0; i < game.numOfPlayers; i++) {
        player_t* player = &game.players[i];

        if (player->isAlive && player->team == team) {

            TurnPlayerIfNecessary(player);

            if (player->dir != 4 && CanPlayerMove(player, player->dir)) {
                coord_t oldPos = player->pos;
                coord_t newPos = InvPM_OffsetCoord(oldPos, player->dir);

                //Move the player
                player->pos.x = newPos.x;
                player->pos.y = newPos.y;

                //Re-draw the spots
                DrawTileAtPos(oldPos);
                DrawTileAtPos(newPos);
            }

            TurnPlayerIfNecessary(player);
            player->pressed_dir = 4; //reset the pressed dir after move
        }

    }

    CheckCollisions(); //Check ghost/player collisions
    ConsumeOrCreateDrops(); //Eat pellets
}

static void TurnPlayerIfNecessary(player_t* player)
{
    if (player->pressed_dir != 4 && CanPlayerMove(player, player->pressed_dir)) {
        player->dir = player->pressed_dir;
        player->pressed_dir = 4;
    }
}

static uint8_t CanPlayerMove(player_t* player, uint8_t dir)
{
    coord_t nextPos = InvPM_OffsetCoord(player->pos, dir);
    if (game.tiles[nextPos.x][nextPos.y].state == WALL) return 0; //Dont let move onto wall

    player_t* nextPlayer = GetPlayerAt(nextPos);
    if (nextPlayer->isAlive && nextPlayer && nextPlayer->team == player->team) return 0; //Dont let teammates move onto eachother

    return 1;
}

static void SetPlayerMoveDir(player_t* player, uint8_t dir)
{
//    if (player->isAlive && CanPlayerMove(player, dir))
//        player->dir = dir;
    player->pressed_dir = dir;
}

static player_t* GetPlayerAt(coord_t pos)
{
    volatile uint8_t i;
    for (i = 0; i < game.numOfPlayers; i++) {
        player_t* player = &game.players[i];
        if (player->isAlive && player->pos.x == pos.x && player->pos.y == pos.y)
            return player;
    }
    return 0;
}

static void CheckCollisions(void)
{
    volatile uint8_t i, j;
    for (i = 0; i < game.numOfPlayers; i++) {
        player_t* player = &game.players[i];
        if (player->isAlive && player->team == TeamPac) {
            for (j = 0; j < game.numOfPlayers; j++) {
                if (i != j) { //Dont check collision with ourself
                    player_t* otherPlayer = &game.players[j];
                    if (otherPlayer->isAlive && player->pos.x == otherPlayer->pos.x && player->pos.y == otherPlayer->pos.y) {
                        switch (otherPlayer->team) {
                            case TeamPac: //same team collision, shouldn't happen as teammates cant move on top of eachother
                                break;
                            case TeamGhost: //opposing team collision
                                switch (otherPlayer->isVulnerable) {
                                    case 0:
                                        KillPlayer(player); //kill the pacman because ghost is not vulnerable
                                        break;
                                    case 1:
                                        KillPlayer(otherPlayer); //kill the ghost because it is vulnerable
                                        break;
                                }
                        }
                    }
                }
            }
        }
    }
}

static void ConsumeOrCreateDrops(void)
{
    volatile uint8_t i;
    for (i = 0; i < game.numOfPlayers; i++) {
        player_t* player = &game.players[i];
        if (player->isAlive) {
           map_tile_t* tile= &game.tiles[player->pos.x][player->pos.y];
           if (tile->drop != NONE)
               tile->drop = PlayerTouchedDrop(player, tile->drop);
           else
               tile->drop = PlayerCanAttemptDrop(player);
        }
        //No need to rerender, it will be done when the player moves off this tile
    }
}

static drop_e PlayerTouchedDrop(player_t* player, drop_e drop)
{
    switch (player->team) {
        case TeamPac:
            switch (drop) {
                case PELLET:
                    game.score_pacmen += PACMEN_SCORE_PER_PELLET;
                    PrintGUI();
                    return NONE;
                case FRUIT:
                    game.score_pacmen += PACMEN_SCORE_PER_FRUIT;
                    PrintGUI();
                    return NONE;
                case POWER_PELLET:
                    game.score_pacmen += PACMEN_SCORE_PER_POWERPELLET;
                    PrintGUI();
                    game.powerTeam = TeamPac; //Pacmen now in power
                    volatile int i;
                    for (i = 0; i < game.numOfPlayers; i++)
                        if (game.players[i].isAlive && game.players[i].team == TeamGhost)
                            game.players[i].isVulnerable = 1;
                    Task_Remove(&ResetGhostsToPower, 0);
                    Task_Schedule(&ResetGhostsToPower, 0, POWER_PELLET_INTERVAL, 0);
                    RerenderAllPlayers(); //re-render so we can see the ghosts are vulnerable
                    return NONE;
            }
    }

    return drop; //keep the same drop
}

static drop_e PlayerCanAttemptDrop(player_t* player)
{
    switch (player->team) {
        case TeamGhost:
            if (random_int(0, 9999)/9999.0 < GHOST_PELLET_DROP_CHANCE) {
                game.score_ghosts += GHOST_SCORE_PER_PELLET_DROP;
                PrintGUI();
                return PELLET;
            }
            if (random_int(0, 9999)/9999.0 < GHOST_FRUIT_DROP_CHANCE) {
                game.score_ghosts += GHOST_SCORE_PER_FRUIT_DROP;
                PrintGUI();
                return FRUIT;
            }
            if (random_int(0, 9999)/9999.0 < GHOST_POWERPELLET_DROP_CHANCE) {
                game.score_ghosts += GHOST_SCORE_PER_POWERPELLET_DROP;
                PrintGUI();
                return POWER_PELLET;
            }
    }

    return NONE;
}

void PrintGUI(void)
{
    Game_CharXY(' ', MAP_WIDTH+1, 1);
    Game_SetColor(Reset_all_attributes);
    Game_Printf("Time left: %d seconds     ", game.secondsLeftInGame); //extra blank space to clear

    Game_CharXY(' ', MAP_WIDTH+1, 3);
    Game_SetColor(ForegroundYellow);
    Game_Printf("Pacmen: %d", game.score_pacmen);

    Game_CharXY(' ', MAP_WIDTH+1, 4);
    Game_SetColor(ForegroundCyan);
    Game_Printf("Ghosts: %d", game.score_ghosts);
}

static void TickGameTimer(void)
{
    if (game.secondsLeftInGame > 0) {
        game.secondsLeftInGame--; // task runs every second so reduce a second.
        PrintGUI();
    } else
        GameOver();
}

static void GameOver(void) {
    // clean up all scheduled tasks
    Task_Remove(&TickGameTimer, 0);
    Task_Remove(&MoveGhosts, 0);
    Task_Remove(&MovePacMen, 0);
    Task_Remove(&RespawnAllPlayers, 0);
    Task_Remove(&ResetGhostsToPower, 0);

    // kill all the players
    volatile int i;
    for (i = 0; i < 4; i++)
        game.players[i].isAlive = 0;

    // set cursor below bottom of map
    Game_CharXY('\r', 0, MAP_HEIGHT + 1);
    // show score
    Game_SetColor(Reset_all_attributes);
    Game_Printf("Game Over! Final Scores: \r\n");
    Game_SetColor(ForegroundYellow);
    Game_Printf("Pacmen: %d\r\n", game.score_pacmen);
    Game_SetColor(ForegroundCyan);
    Game_Printf("Ghosts: %d\r\n", game.score_ghosts);

    // unregister the receiver used to run the game
    Game_UnregisterPlayer1Receiver(&KeyboardReceiver);
    for (i = 0; i < game.numOfPlayers; i++)
        GameControllerHost_RemoveCallback((void (*)(controller_buttons_t, void *))&ControllerReceiver, &game.players[i]);

    // show cursor (it was hidden at the beginning)
    Game_ShowCursor();

    // surrender system control
    Game_GameOver();
}
