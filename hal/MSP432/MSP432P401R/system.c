/*
 * system.c
 *
 *  Created on: Mar 6, 2017
 *      Author: tej
 */

#include "msp.h"
#include "system.h"
#include "spi.h"
#include "nrf24.h"
#include "task.h"
#include "timing.h"

void SYS_Setup()
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD; //Hold watchdog timer

    //Setup clock frequency to FCPU = 24MHz
    CS->KEY = 0x695A; //set password
    CS->CTL0 = CS_CTL0_DCORSEL_4;
    CS->KEY = 0x0000; //clear password
}

void RF1_SetupIO(void)
{
    P2DIR = 0xE0;
}

// This function is provided to the network layer in the init function and is used to control the
// Chip Enable pin on the radio
void RF1_CE(uint8_t out){
    P2OUT = (P2OUT & ~BIT6) | (out << 6);
}

// This function is provided to the network layer in the init function and is used to control the
// Chip Select pin on the radio
void RF1_CSN(uint8_t out){
    P2OUT = (P2OUT & ~BIT5) | (out << 5);
}

uint8_t RF1_GetIRQ()
{
    return (P2IN & BIT7) >> 7;
}
