/*
 * system.h
 *
 *  Created on: Mar 6, 2017
 *      Author: tej
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "library.h"
#include "nrf24.h"

#define FCPU 24000000

//Module definitions
#define USE_MODULE_LIST
#define USE_MODULE_TASK
#define USE_MODULE_UART
#define USE_MODULE_BUFFER
#define USE_MODULE_BUFFER_PRINTF
#define USE_MODULE_GAME_CONTROLLER
#define USE_MODULE_SUBSYS
//#define USE_MODULE_GAME_NFR24

#define USE_SPI_B0
#define SPI_MAX_SIZE 33

//UART Module Settings
#define USE_UART0 //to controller client
#define USE_UART1 //to terminal
#define UART_BAUD 115200  // Run the system at this baud rate
#define UART_TX_BUFFER_LENGTH 2048 // Make the TX buffer thicc

//UART Packet Module settings
#define USE_UPACKET_0 //UART0 (used by game controller host) is used as a packet transfer pipe
#define UPACKET_MAX_LENGTH 16 //Defined max uart packet size. We dont use more than 16 currently...

//Game Controller Host Settings
#define GAME_CONTROLLER_UART UART0 //to controller client. UART0 is set to use packets above

//Subsys Settings
#define SUBSYS_IO SUBSYS_IO_UART //Set the subsystem output type to UART...
#define SUBSYS_UART UART1 //then set the channel of said UART

//NRF Settings
#define RF_SPI_CH SPI_B0

void SYS_Setup(); //Processor specific system setup

void RF1_SetupIO(void);
void RF1_CE(uint8_t out);
void RF1_CSN(uint8_t out);
uint8_t RF1_GetIRQ();

#endif /* SYSTEM_H_ */
