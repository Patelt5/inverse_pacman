/*
 * graphics.c
 *
 *  Created on: Apr 4, 2017
 *      Author: tej
 */

#include "hal_general.h"
#include "graphics.h"
#include "system.h"
#include "uart.h"
#include "terminal.h"

typedef struct {
    g_point_t point;
    g_point_t size;
} point_transform_t;

/******************
 * Helper functions
 ******************/
static point_transform_t Graphics_GetPointTransform(graphics_t* gptr, g_point_t p);
static g_pixel_t Graphics_ConstructPixel(graphics_t* gptr);
static void Graphics_Paint(graphics_t* gptr, g_point_t p);
static void Graphics_PaintPixel(graphics_t* gptr, g_point_t p, g_pixel_t pixel);
static void Graphics_Unpaint(graphics_t* gptr, g_point_t p);
static void Graphics_MarkPixelDirty(graphics_t* gptr, g_point_t p);
static void Graphics_MarkAllPixelsClean(graphics_t* gptr);
static uint8_t Graphics_IsPixelDirty(graphics_t* gptr, g_point_t p);
static void Graphics_RenderChunk(graphics_t* gptr, g_point_t p);
static void Graphics_WaitForTXSpace(graphics_t* gptr, uint16_t size);

/************************************
 * graphics.h function implementation
 ************************************/

void Graphics_Init(graphics_t* gptr)
{
    Terminal_HideCursor(gptr->hal.uart_channel);
    Graphics_ClearScreen(gptr);
}

void Graphics_SetInputScreenSize(graphics_t* gptr, enum screen_size_e screen_size)
{
    gptr->screen_size = screen_size;
}

void Graphics_SetBackground(graphics_t* gptr, uint8_t color[3])
{
    memcpy(&gptr->background[0], &color[0], 3);
}

void Graphics_SetForeground(graphics_t* gptr, uint8_t color[3])
{
    memcpy(&gptr->foreground[0], &color[0], 3);
}

void Graphics_DrawTile(graphics_t* gptr, g_point_t position, g_pixel_t* tile[], char x, char y)
{
    volatile uint8_t i, j;
    for (i = 0; i < x; i++)
        for (j = 0; j < y; j++)
            Graphics_PaintPixel(gptr, (g_point_t){position.x+i, position.y+j}, tile[j][i]);
}

void Graphics_DrawLine(graphics_t* gptr, g_point_t p1, g_point_t p2)
{
    //Bresenham's line algorithm

    volatile int x0 = p1.x, x1 = p2.x;
    volatile int y0 = p1.y, y1 = p2.y;
    int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
    int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1;

    volatile int err = (dx>dy ? dx : -dy)/2, e2;

    while(1) {
        Graphics_Paint(gptr, (g_point_t){(uint16_t)x0, (uint16_t)y0});

        if (x0==x1 && y0==y1) break;
        e2 = err;
        if (e2 >-dx) { err -= dy; x0 += sx; }
        if (e2 < dy) { err += dx; y0 += sy; }
    }
}

void Graphics_DrawPixel(graphics_t* gptr, g_point_t p)
{
    Graphics_Paint(gptr, p);
}

void Graphics_DrawRectangle(graphics_t* gptr, g_point_t top_left, g_point_t bottom_right)
{
    volatile uint16_t x, y;
    for (y = top_left.y; y <= bottom_right.y; y++)
        for (x = top_left.x; x <= bottom_right.x; x++)
            Graphics_Paint(gptr, (g_point_t){x, y});
}

void Graphics_DrawText(graphics_t* gptr, g_point_t position, char* str, ...) //TODO: Draw text with pixels instead of characters to the terminal
{
    // variable argument list type
    va_list vars;
    // initialize the variable argument list pointer by specifying the
    // input argument immediately preceding the variable list
    va_start(vars, str);

    point_transform_t transform = Graphics_GetPointTransform(gptr, position); //Get the transform for the screen

    Terminal_CursorXY(gptr->hal.uart_channel, transform.point.x, transform.point.y);
    UART_vprintf(gptr->hal.uart_channel, str, vars);
    va_end(vars); //end the varg list
}

void Graphics_ClearScreen(graphics_t* gptr)
{
    volatile uint16_t x, y;
    for (y = 0; y < NATIVE_RES_Y; y++)
        for (x = 0; x < NATIVE_RES_X; x++)
            Graphics_Unpaint(gptr, (g_point_t){x, y});

    Graphics_MarkAllPixelsClean(gptr);

    Terminal_ClearScreen(gptr->hal.uart_channel);
}

void Graphics_UpdateScreen(graphics_t* gptr)
{
    volatile uint16_t x, y;
    volatile uint8_t cursorNeedsMove = 1;

    for (y = 0; y < NATIVE_RES_Y; y++) {
        for (x = 0; x < NATIVE_RES_X; x += X_CHUNK_SIZE) {
            //For every chunk in the display buffer...

            if (Graphics_IsPixelDirty(gptr, (g_point_t){x, y})) { //If said chunk needs to be updated...
                if (cursorNeedsMove) { //If we arent already at the start of the chunk...
                    Graphics_WaitForTXSpace(gptr, 8); //Wait for 8 bytes on the tx buffer so we can move the cursor
                    Terminal_CursorXY(gptr->hal.uart_channel, x, y); //Move the cursor there.
                }

                Graphics_RenderChunk(gptr, (g_point_t){x, y}); //Render out the chunk to the terminal

                cursorNeedsMove = 0; //Let the next loop iteration know that it doesnt need to move the cursor
            } else
                cursorNeedsMove = 1; //We skiped over a chunk so we need to move the cursor next time
        }
        cursorNeedsMove = 1;//Force cursor move as we are no longer on the same row
    }
    Graphics_MarkAllPixelsClean(gptr);
}

/********************************
 * Helper function implementation
 ********************************/

static point_transform_t Graphics_GetPointTransform(graphics_t* gptr, g_point_t p)
{
    point_transform_t transform;
    uint16_t w, h;
    switch (gptr->screen_size) {
        case SCREEN_SIZE_128X64:  w = 128; h = 64;  break;
        case SCREEN_SIZE_320X240: w = 320; h = 240; break;
        case SCREEN_SIZE_160X128: w = 160; h = 128; break;
    }

    float fx = (float)p.x/(float)w;
    float fy = (float)p.y/(float)h;
    transform.point.x = fx*NATIVE_RES_X;
    transform.point.y = fy*NATIVE_RES_Y;
    transform.size.x = w > NATIVE_RES_X ? w/NATIVE_RES_X : 1;
    transform.size.y = h > NATIVE_RES_Y ? h/NATIVE_RES_Y : 1;

    return transform;
}

static void Graphics_Paint(graphics_t* gptr, g_point_t p)
{
    Graphics_PaintPixel(gptr, p, Graphics_ConstructPixel(gptr));
}

static g_pixel_t Graphics_ConstructPixel(graphics_t* gptr)
{
    g_pixel_t pixel;

    //scale 8bit colour value to 3bit
    pixel.red   = (uint8_t)(7.0f * gptr->foreground[0]/255.0f);
    pixel.green = (uint8_t)(7.0f * gptr->foreground[1]/255.0f);
    pixel.blue  = (uint8_t)(7.0f * gptr->foreground[2]/255.0f);

    return pixel;
}

static void Graphics_PaintPixel(graphics_t* gptr, g_point_t p, g_pixel_t pixel)
{
    if (pixel.transparent) //If this pixel is transparent just keep the old pixel on the display buffer
        return;

    point_transform_t transform = Graphics_GetPointTransform(gptr, p); //Get the transform for the screen

    volatile uint8_t x, y;
    for (y = transform.point.y; y < transform.point.y + transform.size.y; y++)
        for (x = transform.point.x; x < transform.point.x + transform.size.x; x++) { //for each position in the transform
            gptr->hal.displayBuffer[y][x] = pixel.all; //Set the new pixel to display buffer
            Graphics_MarkPixelDirty(gptr, (g_point_t){x, y}); //Mark pix2 for update
        }
}

static void Graphics_Unpaint(graphics_t* gptr, g_point_t p)
{
    point_transform_t transform = Graphics_GetPointTransform(gptr, p); //Get the transform for the screen

    volatile uint8_t x, y;
    for (y = transform.point.y; y < transform.point.y + transform.size.y; y++)
        for (x = transform.point.x; x < transform.point.x + transform.size.x; x++) { //for each pixel in the transform
            gptr->hal.displayBuffer[y][x] = 0; //Clear the pixel data

            Graphics_MarkPixelDirty(gptr, (g_point_t){x, y}); //Mark pixel as needs to be updated
        }
}

static void Graphics_MarkPixelDirty(graphics_t* gptr, g_point_t p)
{
    gptr->hal.changes[p.y][p.x/X_CHUNK_SIZE] = 1; //Dividing x by chunk size gives the chunk index
}

static void Graphics_MarkAllPixelsClean(graphics_t* gptr)
{
    volatile uint16_t xchunk, y;
    for (y = 0; y < NATIVE_RES_Y; y++)
        for (xchunk = 0; xchunk < X_CHUNK_SIZE; xchunk++)
            gptr->hal.changes[y][xchunk] = 0;
}

static uint8_t Graphics_IsPixelDirty(graphics_t* gptr, g_point_t p)
{
    return gptr->hal.changes[p.y][p.x/X_CHUNK_SIZE] != 0;
}

static void Graphics_RenderChunk(graphics_t* gptr, g_point_t p)
{
    char chunkStr[X_CHUNK_SIZE+1]; //String for the chunk + string terminator

    volatile g_pixel_t pixel;
    volatile uint16_t dx;

    for (dx = 0; dx < X_CHUNK_SIZE; dx++) {
        pixel.all = gptr->hal.displayBuffer[p.y][p.x+dx]; //Load the pixel data

        float brightness = pixel.red/3.0f*0.2126f + pixel.green/3.0f*0.7152 + pixel.blue/3.0f*0.0722; //Calculate a value between 0 and 1 for brightness

        if (brightness <= 0) {
            chunkStr[dx] = ' '; //[SPACE]
        } else if (brightness <= 0.3f) {
            chunkStr[dx] = 176; //░
        } else if (brightness <= 0.6f) {
            chunkStr[dx] = 177; //▒
        } else if (brightness <= 1.0f) {
            chunkStr[dx] = 178; //▓
        }
    }

    Graphics_WaitForTXSpace(gptr, 9);
    UART_Printf(gptr->hal.uart_channel, &chunkStr[0]); //Print the entire chunk string out
}

static void Graphics_WaitForTXSpace(graphics_t* gptr, uint16_t size)
{
    while (UART_TXSpaceAvailable(gptr->hal.uart_channel) < size);
}

