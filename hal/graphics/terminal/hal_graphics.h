/*
 * hal_graphics.h
 *
 *  Created on: Apr 4, 2017
 *      Author: tej
 */

#ifndef HAL_GRAPHICS_H_
#define HAL_GRAPHICS_H_

/**
 * Each row of the terminal window is split into chunks. When a pixel changes
 * the entire chunk is redrawn. A chunk size is chosen so that it is no less
 * efficient to render the whole thing than it is to change the x/y location of
 * the cursor for every pixel. Since it takes about 8 bytes to change the location,
 * using 8 as a chunk size ensures we don't unnecessarily move the cursor around when
 * its simply better to just write the additional bytes to get to the next position.
 */
#define X_CHUNK_SIZE 8

/**
 * Native screen size for the terminal. The game's resolution will
 * be up/down-scaled to this. Size your terminal window to match.
 */
#define NATIVE_RES_X 128
#define NATIVE_RES_Y 64

typedef struct
{
    uint8_t uart_channel; //< The UART channel to the terminal

    /**
     * Display buffer organized as array of rows in top-left origin space.
     *
     * Indexed by [Y, X]
     */
    uint8_t displayBuffer[NATIVE_RES_Y][NATIVE_RES_X];

    /**
     * Holds information on each row that has changed since last
     * screen update. Used so the entire terminal screen does not
     * need to be updated at once. Top-left origin space.
     *
     * Indexed by [Y, X-chunk]
     */
    uint8_t changes[NATIVE_RES_Y][(NATIVE_RES_X+X_CHUNK_SIZE-1)/X_CHUNK_SIZE];
} hal_graphics_t;

#endif /* HAL_GRAPHICS_H_ */
