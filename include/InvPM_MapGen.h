/*
 * InvPM_MapGen.h
 *
 *  Created on: Apr 19, 2017
 *      Author: tej
 */
/**
 * \addtogroup InvPM
 *
 * \author Tej Patel
 *         Autumn Brown
 *         Edward Yates
 *
 * \file InvPM_MapGen.h
 * \date 04/19/2017 File created
 *
 * \breif A random map generator that produces mazes somewhat
 * resembling classic Pacman maps. Maps are always a single
 * corridor wide and contain no dead ends.
 *
 * \bug Depending on the input parameters, there could be
 *      some disconnects in the produced maze. There is no
 *      gaurantee that the map produces will be continuous
 *      (i.e. You can get to one spot on the map to another).
 *
 * @{
 */

#ifndef INVPM_MAPGEN_H_
#define INVPM_MAPGEN_H_

#define GEN_MAX_WIDTH 128   ///< The max width of the map that the generator can produce
#define GEN_MAX_HEIGHT 64   ///< The max height of the map that the generator can produce
#define GEN_MAX_SPAWNERS 32 ///< The max number of spawners that the generator can use to create the map

/**
 * Structure that encapsulates the input parameters that the
 * map generator uses to create the map.
 */
typedef struct
{
    uint32_t seed; ///< The seed that will be used to generate the map. Generation runs
                   ///< will produce the same output if the seed and other parameters are
                   ///< the same. Subsequently, output will differ if the seed does not
                   ///< match even if all other parameters do.

    uint8_t width; ///< The width of the resulting generated map (width <= GEN_MAX_WIDTH)
    uint8_t height; ///< The height of the resulting generated map (height <= GEN_MAX_HEIGHT)
    uint8_t padding; ///< An enforced thickness of the walls surrounding the map (can be zero)

    uint8_t minSpawners; ///< Min number of gen spawners (0 < minSpawners <= maxSpawners)
    uint8_t maxSpawners; ///< Max number of gen spawners (minSpawners <= maxSpawners <= GEN_MAX_SPAWNERS)

    uint8_t minBuilderTravelDist; ///< Min corridor length (0 < minBuilderTravelDist <= maxBuilderTravelDist)
    uint8_t maxBuilderTravelDist; ///< Max corridor length (minBuilderTravelDist <= maxBuilderTravelDist)
} generator_settings_t;

/**
 * An internal encapsulation structure for the two-dimensional array
 * that contains a generated map.
 */
typedef struct {
    /**
     * A generated map indexed by [x, y].
     *
     * A zero value indicates an emtpy space while a
     * non-zero value indicates a wall.
     */
    uint8_t gen_space[GEN_MAX_WIDTH][GEN_MAX_HEIGHT];
} generator_space_t;

/**
 * Used to start generating a map with the given settings
 *
 * \param settings The settings for the generator
 *
 * \return A pointer to a generator_space_t structure containing the resulting map.
 *         Note that this same structure will be used for all calls to this function.
 *         Load the map before calling this function again.
 */
generator_space_t* InvPM_GenerateMap(generator_settings_t* settings);

#endif /* INVPM_MAPGEN_H_ */
