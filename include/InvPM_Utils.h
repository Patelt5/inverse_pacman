/**
 * \addtogroup InvPM
 *
 * \author Tej Patel
 *         Autumn Brown
 *         Edward Yates
 *
 * \file InvPM_Utils.h
 * \date 04/10/2017 File created
 *
 * \brief Contains utility functions used throughout the game
 *
 * @{
 */

#ifndef INVPM_UTILS_H_
#define INVPM_UTILS_H_

/**
 * A simple data structure that represents a position
 * on the game board
 */
typedef struct coord {
    uint8_t x;
    uint8_t y;
} coord_t;

/**
 * Offsets a given x coordinate by a specific direction.
 * \param x The x coordinate to offset
 * \param dir The direction to offset:
 *                0 - Up    ( 0,-1)
 *                1 - Right ( 1, 0)
 *                2 - Down  ( 0, 1)
 *                3 - Left  (-1, 0)
 * \return The x position after the offset
 */
uint8_t InvPM_OffsetX(uint8_t x, uint8_t dir);

/**
 * Offsets a given y coordinate by a specific direction.
 * \param y The y coordinate to offset
 * \param dir The direction to offset:
 *                0 - Up    ( 0,-1)
 *                1 - Right ( 1, 0)
 *                2 - Down  ( 0, 1)
 *                3 - Left  (-1, 0)
 * \return The y position after the offset
 */
uint8_t InvPM_OffsetY(uint8_t y, uint8_t dir);

/**
 * Used to obtain a coordinate after offsetting it towards
 * a certain direction.
 * \param pos The coordinate to offset
 * \param dir The direction to offset:
 *                0 - Up    ( 0,-1)
 *                1 - Right ( 1, 0)
 *                2 - Down  ( 0, 1)
 *                3 - Left  (-1, 0)
 * \return A new coordinate in the position of the offset
 */
coord_t InvPM_OffsetCoord(coord_t pos, uint8_t dir);

/**
 * Used to mutate a coordinate towards a certain direction.
 * \param pos The coordinate to mutate
 * \param dir The direction to offset:
 *                0 - Up    ( 0,-1)
 *                1 - Right ( 1, 0)
 *                2 - Down  ( 0, 1)
 *                3 - Left  (-1, 0)
 */
void InvPM_MoveCoord(coord_t* pos, uint8_t dir);

/**
 * A blocking function that can be used to wait for space to clear up
 * on the TX Buffer for a given UART channel.
 *
 * \param channel The UART channel whose TX buffer space is in question
 * \param size The desired space to be available on the TX buffer
 *             after the function returns.
 */
void InvPM_WaitForTXSpace(uint8_t channel, uint16_t size);

#endif /* INVPM_UTILS_H_ */

/// @}
