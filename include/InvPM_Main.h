/**
 * \defgroup InvPM Inverse Pacman
 *
 * \author Tej Patel
 *         Autumn Brown
 *         Edward Yates
 *
 * \file InvPM_Main.h
 * \date 04/08/2017 File created
 *
 * \brief A twist on the classic game of Pacman.
 *
 * Game of Pacman where multiple players control both the ghosts and the Pacman.
 * The ghosts move around the map and drop pellets to score points, while the
 * Pacmen attempt to collect them for points.
 *
 *

    <b>Description of the Initial Goals and Motivation of the project. </b> <br>
    Our initial goal was to create a game that resembled pacman but
    at the same time was not the traditional pacman game that we all
    know and love. We thought about how to make the game look and feel
    like pacman without being too similar or too different. Thus we
    came up with the idea of inverse pacman, which puts a different
    twist on the classic game. This game puts the players in the role
    of the ghosts chasing a player controlled pacman. The ghosts must
    work together to corner and trap the pacman to gain points,
    while at the same time trying to outscore the rapidly scoring pacman
    who also gets a chance at revenge with the famous power pellet.
    Motivation for the game is derived from the desire to create a fun
    and enjoyable multiplayer experience that comes from classic arcade
    style games. Everyone enjoys going to the arcade and spending many
    quarters trying to best their friends in light hearted competitive
    gameplay, and that is the experience we wanted to emulate.

    <b>Description of the operation and use of your project. </b> <br>
    Our game utilizes many different devices in order to produce the final
    game mechanics. For starters, the game itself is run on an MSP432
    launchpad due to it being a bit more capable than that of an regular
    MSP430. However, due to this being one of the novel selections, a Hardware
    Abstraction Layer (HAL) needed to be created in order to communicate with
    the launchpad. The controller that was selected for the players to use was
    that of a playstation 2. We decided to take it a little bit further by
    choosing a wireless version of this controller, which in reality was no
    more challenging to implement than a regular wired one. This controller
    was operated using an MSP430F5529 for each desired PS2 controller. For
    the players that were not able to use a controller, keyboard inputs were
    provided in order to control their character. In the end, one controller
    was used to control the pacman player, while the two-three ghost players
    used "WASD" "TFGH" and "IJKL" respectively.


    <b>Discussion of the final results and conclusion of your project. </b><br>
    The final results of the project were mainly a success. The basic
    goals and requirements of the game were met while some of the more
    extensive parts unfortunately fell short. However, these shortcomings
    had little impact on the overall performance of the game. Unfortunately
    the wireless functionality of the game was not achieved so all players
    had to play on device and display. Also, due to certain hardware
    failure, only one controller was able to be implemented despite various
    efforts. In spite of some of these minor difficulties, the game received
    numerous compliments and many of those who played thoroughly enjoyed the
    experience. Students were eager to get a chance to play and visibly
    had a fun time in doing so. In the end, the classic feeling of playing an
    old fashioned style competive arcade game with a few friends was accomplished.


    <b>How helpful is Slack? </b> <br>
    Slack provides a means for a group or project team to effectively
    communicate with each other and discuss pertinent project information.
    However, in a team with few members, it is practically natural to
    communicate through a casual means such as text messages or email.
    Using a program specifically designed for communication seems almost
    forced and almsot like an additional task to do rather than feeling
    like natural communicaiton. In the case of our team, there were only
    three members so getting in contact and staying organized was easy
    and efficient simply by texting or through email. However, if the group
    size was larger and there were many more tasks, Slack definitely shows
    its usefulness as it is able to allow teams to separate into different
    channels and all communication and information regarding that task
    can be organized into that one area. But for smaller sized teams
    it does not make that much of an impact.


    <b>How helpful is Trello? </b> <br>
    Trello is a great tool to help keep teams on track. Follwing the scrum
    techniques of bi weekly objectives and scrum meetings is really well
    executed with the help of Trello. All of that week's assigned tasks are
    easy to see and keep track of who still needs to fulfill their parts.
    It also keeps a backlog of all the things that were previosuly completed
    in order to know what works and what does not as well as being able to
    identify a problem that may have occurred after a certain task was
    finsihed. Overall, Trello widely helps teams stay organized and focused
    during multi week projects.

 * @{
 */

#ifndef INVPM_MAIN_H_
#define INVPM_MAIN_H_

/**** Map Generator Settings - SETTINGS MUST BE IDENTICAL FOR HOST AND CLIENT ****/

#define MAP_WIDTH               37 ///< The width of the physical map. Cleaner generations with (MAP_WIDTH+2*MAP_PADDING) being an odd number.
#define MAP_HEIGHT              19 ///< The height of the physical map. Cleaner generations with (MAP_HEIGHT+2*MAP_PADDING) being an odd number.
#define MAP_PADDING              1 ///< The enforced thickness of the walls surrounding the map. MUST be at least 1.
#define MAP_MIN_INTRICACY       32 ///< Controls min number of spawners in the generation algorithm. See InvPM_MapGen.h
#define MAP_MAX_INTRICACY       32 ///< Controls max number of spawners in the generation algorithm. See InvPM_MapGen.h
#define MAP_MIN_CORRIDOR_DIST    2 ///< The min corridor distance in the generated map.
#define MAP_MAX_CORRIDOR_DIST    4 ///< The max corridor distance in the generated map.

/**** Timing Settings ****/

#define GAME_DURATION_SECONDS     120 ///< Number of seconds that the game lasts
#define PAC_MOVEMENT_INTERVAL     180 ///< Milliseconds between pacman movement
#define GHOST_MOVEMENT_INTERVAL   180 ///< Milliseconds between ghost movement
#define RESPAWN_PASS_INTERVAL    5000 ///< Number of ms between respawning all players
#define POWER_PELLET_INTERVAL   10000 ///< Milliseconds that the Power Pellet lasts

/**** Random Probability Settings ****/

#define PELLET_SPAWN_CHANCE           0.0800 ///< Percent chance of a pellet to spawn on a square at the start of the game
#define GHOST_PELLET_DROP_CHANCE      0.1500 ///< Percent chance of a ghost to drop a pellet per move
#define GHOST_FRUIT_DROP_CHANCE       0.0080 ///< Percent chance of a ghost to drop a fruit per move
#define GHOST_POWERPELLET_DROP_CHANCE 0.0035 ///< Percent chance of a ghost to drop a power pellet per move

/**** Points and Score Settings ****/

#define GHOST_SCORE_PER_KILL             30  ///< Points scored by ghosts for killing PacMan
#define GHOST_SCORE_PER_PELLET_DROP       1  ///< Points scored by ghosts when dropping a pellet
#define GHOST_SCORE_PER_FRUIT_DROP       10  ///< Points scored by ghosts when dropping fruit
#define GHOST_SCORE_PER_POWERPELLET_DROP 20  ///< Points scored by ghosts when dropping a power pellet
#define PACMEN_SCORE_PER_KILL            65  ///< Points scored by pacman when killing a ghost
#define PACMEN_SCORE_PER_PELLET           2  ///< Points scored by pacman when eating a pellet
#define PACMEN_SCORE_PER_FRUIT           10  ///< Points scored by pacman when eating a fruit
#define PACMEN_SCORE_PER_POWERPELLET      0  ///< Points scored by pacman when eating a powerpellet

/**
 * Used to register the game with the game management module and
 * do any one-time setup.
 */
void InvPM_Init(void);

#endif /* INVPM_MAIN_H_ */

///@}
